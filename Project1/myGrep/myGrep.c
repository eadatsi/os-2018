#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char *argv[]) { 
	//Multiple iterations till all text files are read and printed

	if (argv[1] == NULL){
		printf("my-grep: searchterm [file...]\n");
		return(1);
	}


	//Search string input is an empty string
	//print the whole file
	if (argv[1] == ""){
		int i = 2;
		char words[2064];

		//File Pointer
		FILE *filept;

		//Open file. One file at a time
		while(argv[i] != NULL){

			filept = fopen(argv[i], "r");

			if (filept == NULL){
				printf("my-cat: Cannot open file\n");
				return(1);

			}else{
				printf("-------------------------No Search Item------------------------------");
				//printf("\nNo Search Item was entered!  \n We are giving you the whole deal:::");
				//printf("---------------------Here We Go--------------------------------------");
				while(fgets(words, 2064, filept)!= NULL){
					//put in words
					printf("%s", words);
					}
					//Close the file
					fclose(filept);
				}
			i++;
		}
		

		return(0);

	}

	//When a search item is provided with no file
	//Accept a new string and search for the word in that file 

	if (argv[1] != NULL && argv[2] == NULL){
		char strIn[2000];
		
		printf("\n Enter a file name:   ");
		fgets(strIn, 2000, stdin);

		printf("Printing------>%s\n", strIn);

		for(int i=0; i<strlen(strIn); i++){
			if(strIn[i] == '\n') strIn[i] ='\0';
		}

		//File Pointer
		FILE *filept;

		while (strIn != NULL){
		//Open file. One file at a time
			filept = fopen(strIn, "r");
	
		//If there is no file
		    if (filept == NULL){
				printf("my-grep: Cannot open file\n");
				return(1);
			}else{
				char words[2064];
				while(fgets(words, 2064, filept)!= NULL){
					
					//Compare the string and each line
					if((strstr(words, argv[1])) != NULL){
							//Print the line
						printf("%s", words);
						
					}
				}	
			
			}
		//Close the file
			fclose(filept);
			return(0);
			//Counter increment in case there are multiple files to read and write out		
		}	
	return(0);
	}

	//Initialize counter
	int i = 2;
	while (argv[i] != NULL){
		//initializing char variable to hold 500 characters
		char words[2064];

		//File Pointer
		FILE *filept;

		//Open file. One file at a time
		filept = fopen(argv[i], "r");
	
		//If there is no file
		if (filept == NULL){

			printf("my-grep: Cannot open file\n");
			return(1);

		}else{

			while(fgets(words, 2064, filept)!= NULL){
			
				//Compare the string and each line
                if((strstr(words, argv[1])) != NULL){
                    //Print the line
					printf("%s", words);
                    
				}
			}
			//Close the file
			fclose(filept);
		}
		//Counter increment in case there are multiple files to read and write out
		i++;
	}
	return(0);
}