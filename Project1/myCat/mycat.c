#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[]) { 
	//Multiple iterations till all text files are read and printed
	int i = 1;
	int ch;

	if (argv[1] == NULL){
		return(0);
	}

	while (argv[i] != NULL){
		//initializing char variable to hold 500 characters
		char words[2064];

		//File Pointer
		FILE *filept;

		//Open file. One file at a time
		filept = fopen(argv[i], "r");
	
		//If there is no file
		if (filept == NULL){
			printf("my-cat: Cannot open file\n");
			return(1);

		}else{
			while(fgets(words, 2064, filept)!= NULL){
				//put in words
				printf("%s", words);
			}
			//Close the file
			fclose(filept);
		}
		//Counter increment in case there are multiple files to read and write out
		i++;
	}
	return(0);
}
	



