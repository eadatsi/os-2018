#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

char *read_wish(void);
char **split_wish(char *strIN);
void wish_loop(void);
int execute_wish(char **args);
int batch_exe(char **args);


void err_msg(void){
    char error_message[30] = "An error has occurred\n";
    write(STDERR_FILENO, error_message, strlen(error_message));
}
/*
    Collect the Input from the user and return it.
*/
char *read_wish(void){
    size_t leng = 0;
    char * strIN = NULL;

    getline(&strIN, &leng, stdin);

    for(int i=0; i<strlen(strIN); i++){
        if(strIN[i] == '\n') strIN[i] ='\0';
         }
    return strIN;
}

/*
    Code for Parsing the User's Input
*/
#define max_line 20
#define delimiter " \n\t\r\a"
char **split_wish(char *strIN){
    int buffsize = max_line;
    int count=0;
    char **tokens_array = malloc(buffsize * sizeof(char*));
    char *present_token;

    //Exit if space allocation was not successfulr
    if (!tokens_array){
        err_msg();
        wish_loop();
    }

    //splitting the inputs into blocks
    present_token = strtok(strIN, delimiter);
    while (present_token != NULL){
        //per index, populate the array
        tokens_array[count] = present_token;
        count ++;

        //Memory reallocation
        if (count >= buffsize){
            buffsize = buffsize + max_line;
            tokens_array = realloc(tokens_array, buffsize * sizeof(char*));
            //Exit if allocation was not successful
            if(!tokens_array){
                err_msg();
                wish_loop();
            }
        }
        present_token = strtok(NULL, delimiter);
    }
    tokens_array[count] = NULL;
    return tokens_array;
}

/*
    Code for loop that basically controls the program
*/
void wish_loop(void){
    
    char *input;
    char **wish_arg;
    int status;

    do{
        printf("wish>");
        input= read_wish();
        wish_arg = split_wish(input);
        status = execute_wish(wish_arg);

        free(input);
        free(wish_arg);
    } 
    while(status); 
}

/*
    Code for handling the Processes, Fork, exec and wait!
*/
int process_handling(char **args){
    int status;
    pid_t wpid;

    int rc = fork();
    if (rc == 0){
        //Child Process
        if(execvp(args[0],args) == -1){
            err_msg();
        }exit(0);
    }
        else if (rc < 0){
            //error occured with creating the child process
            err_msg();
        }else{
            // Parent process is in control
            do{
                wpid = waitpid(rc, &status, WUNTRACED);
            }while (!WIFEXITED(status) && !WIFSIGNALED(status));
        }
        return 1;
    }
    


/* 
    Code for Built-Ins
*/
char *builtins[] = {"exit", "cd", "path"};
//-------------------------------------
int exit_imp(char **args){
    printf("Exiting......\n");
    exit(0);
}
//-------------------------------------
void cd_imp(char **args){
    if (args[0] == NULL){
        err_msg();
    }else{
        if (chdir(args[1]) != 0){
            err_msg();
        }else{
            printf("Directory has been changed to: %s\n", args[1]);
        }
    }
}
//-------------------------------------
//Path change implementation
int path_imp(char **args){

    char str[1024];
    strcpy(str, args[1]);
    char *colon = ":";
    int i = 2;
    if (args[i] != NULL){
        strcat(str, colon);
        strcat(str, args[i]);
        i++;
    }

    setenv("PATH",str,1);
    wish_loop(); 
}


//-------------------------------------
//    Redirection Implementation

int Redir(char **args, char *filename){
    
    int out;
    pid_t p1; 

    //Creating the processes 
    p1 = fork(); 
    if (p1 < 0) { 
        printf("\nCould not fork"); 
        return 0; 
    } 

    if (p1 == 0){     
    // open input and output files
    freopen(filename, "w+", stdout); //Open and write to std out
    char result;
    result = execvp(args[0], args); //Execution
    exit(-1);
    } wait(NULL);
}

//-------------------------------------
/*
    Code for General Execution
*/
int checkerBuiltin(char **args){
    if (strcmp(args[0], "cd")== 0){
        cd_imp(args);
        if(args[1] == NULL){
            err_msg;
            wish_loop();
        }
    }else if (strcmp(args[0], "exit")== 0){
        exit_imp(args);
    }else if(strcmp(args[0], "path")== 0){
        path_imp(args);
    }else{
        err_msg();
        wish_loop();
    }
}

int execute_wish(char **args){

    if (args[0] == NULL){
        //Empty command was entered
        wish_loop();
    }

    //Checking for builtin commands
    int a = 0;
    for (a = 0; a < (sizeof(builtins) / sizeof(char *)); a++) {
        if (strcmp(args[0], builtins[a]) == 0) {
            return checkerBuiltin(args);
        }  
    }

    //Parallel commands
    
    //Redirection Check

    // Figure out how to run process handling if strcmp for > fails 
    
   int i,j;
   char *output_file;

    for(i = 0; args[i] != NULL; i++) {
    // Look for the >
    if(args[i][0] == '>') {
      // Get the filename 
      if(args[i+1] != NULL) {
	output_file = args[i+1];
      } else {
	return -1;
      }
      // Adjust the rest of the arguments in the array
      for(j = i; args[j-1] != NULL; j++) {
	    args[j] = args[j+2];
      }
    }
  }   

    return Redir(args , output_file);  
    return process_handling(args);
}

/*
        Code for Batch Mode
*/
int batch_exe(char **args){
        printf("batch Mode activated! \n");
        printf("Executing.... \n");

        //Remove new line character from entry
        char *input = args[1];
        for(int i=0; i<strlen(input); i++){
			if(input[i] == '\n') input[i] ='\0';
		}
        //File Pointer
		FILE *filept;

		filept = fopen(input, "r");
        //If there is no file
		    if (filept == NULL){
                //Delete this print function after
                printf("No file found");
				err_msg();
			}
            else{
				char *words = NULL;
                size_t leng = 0;
                ssize_t len = 0;
                char **lines;
                
				while(len = getline(&words, &leng, filept) >= 0){      
                    lines = split_wish(words);
                    execute_wish(lines);				
					}
                	
			}
}

//-------------------------------------
/*
    Final Main Code which calls and implements all the functions
 */
int main(int argc, char **argv){

    //Check for batch mode first!
    if (argv[1] == NULL){
        wish_loop();
    }
    else if (argv[2] != NULL){ 
        printf("Bad Batch File\n");
        printf("Exiting....\n"); 
        exit(1);    
    }else{
        batch_exe(argv);
    }
}